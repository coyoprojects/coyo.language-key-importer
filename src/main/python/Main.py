#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""COYO language key importer - Main"""

import os
import time
import logging
import json
from pathlib import Path

from lib.Utils import read_json_from_file
from lib.coyo.CoyoOps import CoyoOps
from lib.coyo.CoyoData import CoyoData
from lib.AppConfig import AppConfig
from gui.MainGui import GUI

# COYO settings
BASE_URL = ''
USERNAME = ''
PASSWORD = ''
CLIENTID = ''
CLIENTSECRET = ''
MULTIBACKEND = False
SESSIONNAME = ''
PREFER_UPDATE_REQUESTS = False

# Logging configuration
logging_logtofile = True
logging_loglevel = logging.DEBUG
logging_datefmt = '%d-%m-%Y %H:%M:%S'
logging_format = '[%(asctime)s] [%(levelname)-5s] [%(module)-20s:%(lineno)-4s] %(message)s'
logging_logfile = str(Path.home()) + '/logs/coyo-language-key-importer.application-' + time.strftime('%d-%m-%Y-%H-%M-%S') + '.log'

def _initialize_logger():
    """Initializes the logging utility"""
    logging.basicConfig(level=logging_loglevel,
                        format=logging_format,
                        datefmt=logging_datefmt)

    if logging_logtofile:
        basedir = os.path.dirname(logging_logfile)

        if not os.path.exists(basedir):
            os.makedirs(basedir)
        handler_file = logging.FileHandler(logging_logfile, mode='w', encoding=None, delay=False)
        handler_file.setLevel(logging_loglevel)
        handler_file.setFormatter(logging.Formatter(fmt=logging_format, datefmt=logging_datefmt))
        logging.getLogger().addHandler(handler_file)

if __name__ == '__main__':
    _initialize_logger()

    appconfig = AppConfig()
    coyodata = CoyoData(
        baseurl=BASE_URL,
        username=USERNAME,
        password=PASSWORD,
        clientid=CLIENTID,
        clientsecret=CLIENTSECRET,
        multibackend=MULTIBACKEND,
        sessionname=SESSIONNAME,
        prefer_update_requests=PREFER_UPDATE_REQUESTS)
    gui = GUI(appconfig, coyodata)
