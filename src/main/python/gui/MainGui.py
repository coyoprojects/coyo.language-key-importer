#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""COYO language key importer - GUI"""

import logging
import sys

from lib.coyo.CoyoData import CoyoData
from gui.components.AppContext import AppContext

class GUI():
    """Main GUI"""

    def __init__(self, appconfig, coyodata):
        """Initializes the GUI

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        """
        logging.debug('Initializing MainGUI')

        appctxt = AppContext(appconfig, coyodata)
        exit_code = appctxt.run()
        sys.exit(exit_code)
