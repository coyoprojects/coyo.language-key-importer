#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""COYO language key importer - Main widget"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QMessageBox, QDesktopWidget, QGridLayout, QLabel, QLineEdit, QPushButton, QFileDialog, QCheckBox, QComboBox

from lib.Utils import read_json_from_file
from lib.coyo.CoyoOps import CoyoOps
from lib.coyo.CoyoData import CoyoData
from lib.coyo.CoyoLanguage import CoyoLanguage
from lib.exceptions.LanguageParsingException import LanguageParsingException
from gui.threads.UploadThread import UploadThread

class MainWidget(QWidget):
    """Main widget GUI"""

    def __init__(self, appconfig, log, coyodata):
        """Initializes the main widget

        :param appconfig: The AppConfig
        :param log: The (end user) message log
        :param coyodata: The CoyoData
        """
        super().__init__()

        logging.debug('Initializing MainWidget')

        self.appconfig = appconfig
        self.log = log
        self.components = []
        self.coyodata = coyodata
        self.filename = None
        
        self.font_label_header = QFont()
        self.font_label_header.setBold(True)

        self.threadpool = QThreadPool()
        logging.debug('Multithreading with maximum {} threads.'.format(self.threadpool.maxThreadCount()))

    def init_ui(self):
        """Initiates application UI"""
        logging.debug('Initializing MainWidget GUI')

        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_empty = QLabel('')
        self.label_details_server = QLabel('Server details')
        self.label_details_settings = QLabel('Settings')
        self.label_details_language = QLabel('Language')
        self.label_details_actions = QLabel('Actions')
        self.label_baseurl = QLabel('Base URL')
        self.label_username = QLabel('Username')
        self.label_password = QLabel('Password')
        self.label_clientid = QLabel('Client ID')
        self.label_clientsecret = QLabel('Client Secret')
        self.label_multibackend = QLabel('Multi-backend')
        self.label_sessionname = QLabel('Session name')
        self.label_prefer_update_request = QLabel('Prefer updating')
        self.label_language = QLabel('Language')
        self.label_selected_file_label = QLabel('Selected file')
        self.label_selected_file = QLabel('-')

        self.label_details_server.setFont(self.font_label_header)
        self.label_details_settings.setFont(self.font_label_header)
        self.label_details_language.setFont(self.font_label_header)
        self.label_details_actions.setFont(self.font_label_header)

        self.edit_baseurl = QLineEdit()
        self.edit_baseurl.setText(self.coyodata.baseurl)
        self.edit_username = QLineEdit()
        self.edit_username.setText(self.coyodata.username)
        self.edit_password = QLineEdit()
        self.edit_password.setText(self.coyodata.password)
        self.edit_clientid = QLineEdit()
        self.edit_clientid.setText(self.coyodata.clientid)
        self.edit_clientsecret = QLineEdit()
        self.edit_clientsecret.setText(self.coyodata.clientsecret)

        self.checkbox_multibackend = QCheckBox()
        self.checkbox_multibackend.setChecked(self.coyodata.multibackend)
        self.checkbox_multibackend.stateChanged.connect(self._checkbox_multibackend_statechanged)
        self.edit_sessionname = QLineEdit()
        self.edit_sessionname.setText(self.coyodata.sessionname)
        self.checkbox_prefer_update_requests = QCheckBox()
        self.checkbox_prefer_update_requests.setChecked(self.coyodata.prefer_update_requests)

        self.combobox_language = QComboBox()
        self.combobox_language.addItems([language.name for language in CoyoLanguage])

        self.button_select_file = QPushButton('Select file')
        self.button_select_file.clicked[bool].connect(self._select_file)
        self.button_start = QPushButton('Start upload')
        self.button_start.clicked[bool].connect(self._start_preparing_processing)

        self.components.append(self.edit_baseurl)
        self.components.append(self.edit_username)
        self.components.append(self.edit_password)
        self.components.append(self.edit_clientid)
        self.components.append(self.edit_clientsecret)
        self.components.append(self.checkbox_multibackend)
        self.components.append(self.checkbox_prefer_update_requests)
        self.components.append(self.edit_sessionname)
        self.components.append(self.combobox_language)
        self.components.append(self.button_select_file)
        self.components.append(self.button_start)

        curr_gridid = 1

        self.grid.addWidget(self.label_details_server, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_baseurl, curr_gridid, 0)
        self.grid.addWidget(self.edit_baseurl, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_username, curr_gridid, 0)
        self.grid.addWidget(self.edit_username, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_password, curr_gridid, 0)
        self.grid.addWidget(self.edit_password, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_clientid, curr_gridid, 0)
        self.grid.addWidget(self.edit_clientid, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_clientsecret, curr_gridid, 0)
        self.grid.addWidget(self.edit_clientsecret, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_multibackend, curr_gridid, 0)
        self.grid.addWidget(self.checkbox_multibackend, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_sessionname, curr_gridid, 0)
        self.grid.addWidget(self.edit_sessionname, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_settings, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_prefer_update_request, curr_gridid, 0)
        self.grid.addWidget(self.checkbox_prefer_update_requests, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_language, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_language, curr_gridid, 0)
        self.grid.addWidget(self.combobox_language, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_selected_file_label, curr_gridid, 0)
        self.grid.addWidget(self.label_selected_file, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.button_select_file, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_actions, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_start, curr_gridid, 0, 1, 3)

        self.setLayout(self.grid)
        self._reset_enabled()

    def _checkbox_multibackend_statechanged(self):
        """On checkbox 'multibackend' change"""
        ischecked = self.checkbox_multibackend.isChecked()
        self.edit_sessionname.setEnabled(True if ischecked else False)

    def _select_file(self):
        """File selection dialog"""
        logging.debug('Select file')
        options = QFileDialog.Options()
        self.filename, _ = QFileDialog.getOpenFileName(self, 'Select a language file', '', 'JSON Files (*.json)', options=options)
        if self.filename:
            logging.info('Reading from file "{}"'.format(self.filename))
            self.log('Reading from file "{}"'.format('...' + self.filename[-55:]))
            self.label_selected_file.setText('...' + self.filename[-60:])

    def _reset_enabled(self):
        """Resets all component to initial state"""
        logging.debug('Resetting components to enabled state')

        self._enable()
        self._checkbox_multibackend_statechanged()

    def _disable(self):
        """Resets all component to disabled state"""
        logging.debug('Disabling components')

        for comp in self.components:
            comp.setEnabled(False)

    def _enable(self):
        """Resets all component to enabled state"""
        logging.debug('Enabling components')

        for comp in self.components:
            comp.setEnabled(True)

        self._checkbox_multibackend_statechanged()

    def show_dialog_critical(self, window_title, text, inf_text, detail_text):
        """Shows a dialog, critical

        :param window_title: The window title
        :param text: The text
        :param inf_text: The information text
        :param detail_text: The detail text
        """
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setStandardButtons(QMessageBox.Ok)

        msg.setWindowTitle(window_title)
        msg.setText(text)
        msg.setInformativeText(inf_text)
        msg.setDetailedText(detail_text)
        
        return msg.exec_()

    def _start_preparing_processing(self):
        """Starts the processing process"""
        logging.debug('Starting preparing processing')

        try:
            self._disable()
            errors = []
            baseurl = self.edit_baseurl.text()
            if not baseurl:
                logging.error('Invalid base URL')
                errors.append('Please enter a base URL')
            username = self.edit_username.text()
            if not username:
                logging.error('Invalid username')
                errors.append('Please enter a username')
            password = self.edit_password.text()
            if not password:
                logging.error('Invalid password')
                errors.append('Please enter a password')
            clientid = self.edit_clientid.text()
            if not clientid:
                logging.error('Invalid client ID')
                errors.append('Please enter a client ID')
            clientsecret = self.edit_clientsecret.text()
            if not clientsecret:
                logging.error('Invalid client secret')
                errors.append('Please enter a client secret')
            if self.checkbox_multibackend.isChecked() and not self.edit_sessionname.text():
                logging.error('Invalid session name')
                errors.append('Please enter a valid session name if multi-backend mode is selected')
            if not self.filename:
                logging.error('No file selected')
                errors.append('Please select a file containing the language keys')
            else:
                self.keys = read_json_from_file(self.filename)
                if not self.keys:
                    logging.error('Failed to parse JSON from file "{}"'.format(self.filename))
                    errors.append('Failed to parse JSON from file "{}"'.format(self.filename))
            if errors:
                err_str = ''
                for i, err in enumerate(errors):
                    if i > 0:
                        err_str += '\n'
                    err_str += '- ' + err
                self.show_dialog_critical('Error', 'Parameters missing', 'Some basic parameters are missing.', err_str)
                self._reset_enabled()
            else:
                self.coyodata = CoyoData(baseurl=baseurl,
                                         username=username,
                                         password=password,
                                         clientid=clientid,
                                         clientsecret=clientsecret,
                                         multibackend=self.checkbox_multibackend.isChecked(),
                                         sessionname=self.edit_sessionname.text(),
                                         prefer_update_requests=self.checkbox_prefer_update_requests.isChecked())
                self._start_processing()
        except Exception:
            logging.exception('Could not start processing')
            self.log('Could not start processing.')
            self._reset_enabled()

    def _keydone(self, nr_processed, keys_null, keys_failed):
        """The intermediate processing callback, on keydone

        :param nr_processed: The number of keys processed, yet
        :param keys_null: Null keys
        :param keys_failed: Failed keys
        """
        logging.info('Status: {}% ({}/{} keys, {} null, {} failed)'
            .format(round(nr_processed / len(self.keys) * 100),
                    nr_processed,
                    len(self.keys),
                    len(keys_null),
                    len(keys_failed)))
        self.log('Status: {}% ({}/{} keys, {} null, {} failed)'
            .format(round(nr_processed / len(self.keys) * 100),
                    nr_processed,
                    len(self.keys),
                    len(keys_null),
                    len(keys_failed)))

    def _callback_processing_result(self, nr_processed, keys_null, keys_failed):
        """The processing callback, on result

        :param nr_processed: The number of keys processed, yet
        :param keys_null: Null keys
        :param keys_failed: Failed keys
        """
        logging.debug('Callback: Processing result')
        logging.info('Status: 100% ({}/{} keys, {} null, {} failed)'
            .format(nr_processed,
                    len(self.keys),
                    len(keys_null),
                    len(keys_failed)))
        logging.info('Null values:')
        logging.info('{}'.format(json.dumps(keys_null, ensure_ascii=False)))
        logging.info('Failed:')
        logging.info('{}'.format(json.dumps(keys_failed, ensure_ascii=False)))
        self.log('Status: 100% ({} {}, {} null, {} failed)'
            .format(nr_processed,
                    'keys' if nr_processed != 1 else 'key',
                    len(keys_null),
                    len(keys_failed)))
        self._enable()

    def _callback_processing_error(self, ex):
        """The processing callback, on error
        
        :param ex: The exception thrown
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to upload language keys: "{}"'.format(ex))
        self.log('Failed to upload language keys.')
        self.show_dialog_critical('Error', 'Upload failed', 'Failed to upload language keys: "{}"'.format(ex), None)
        self._reset_enabled()

    def _callback_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

    def _start_processing(self):
        """Starts the processing"""
        logging.debug('Starting processing')

        try:
            clanguage = self.combobox_language.currentText()
            try:
                language = CoyoLanguage[clanguage]
            except Exception:
                raise LanguageParsingException(clanguage)
            logging.info('Set language to "{}"'.format(language.name))

            ops = CoyoOps(self.appconfig, self.coyodata)
            ops.init()
            if ops.valid:
                logging.info('Uploading language keys...')
                self.log('Uploading language keys...')
                thread = UploadThread(coyodata=self.coyodata, ops=ops, keys=self.keys, language=language)
                thread.signals.result.connect(self._callback_processing_result)
                thread.signals.keydone.connect(self._keydone)
                thread.signals.error.connect(self._callback_processing_error)
                thread.signals.finished.connect(self._callback_processing_finished)
                self.threadpool.start(thread)
            else:
                logging.error('Could not retrieve access token.')
                self.log('Could not retrieve access token.')
                self._reset_enabled()
                self.show_dialog_critical('Error', 'Access token', 'Could not retrieve access token.', None)
        except LanguageParsingException as lpe:
            logging.exception('Could not parse language: "{}"'.format(lpe))
            self.log('Language is not valid: "{}"'.format(lpe))
            self.show_dialog_critical('Error', 'Language', 'Language is not valid: "{}"'.format(lpe), None)
            self._reset_enabled()
        except Exception as ex:
            logging.exception('Could not start processing: "{}"'.format(ex))
            self.log('Could not start processing.')
            self.show_dialog_critical('Error', 'Error', 'Could not start processing: "{}"'.format(ex), None)
            self._reset_enabled()
