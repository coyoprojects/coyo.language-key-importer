#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""COYO language key importer - Worker Signals"""

from PyQt5.QtCore import QObject, pyqtSignal

class WorkerSignals(QObject):
    """
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        `object` the exception, anything

    keydone
        `int` number of keys processed, int
        `object` keys null until now, anything
        `object` keys failed until now, anything

    result
        `int` number of keys processed, int
        `object` keys null until now, anything
        `object` keys failed until now, anything
    """
    finished = pyqtSignal()
    error = pyqtSignal(object)
    keydone = pyqtSignal(int, object, object)
    result = pyqtSignal(int, object, object)
