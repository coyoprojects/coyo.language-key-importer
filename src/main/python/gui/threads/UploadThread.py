#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""COYO language key importer - Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from lib.coyo.CoyoLanguage import CoyoLanguage
from gui.signals.WorkerSignals import WorkerSignals

class UploadThread(QRunnable):
    """Upload thread"""

    def __init__(self,
                 coyodata,
                 ops,
                 keys,
                 language=CoyoLanguage.EN):
        """Initializes the thread

        :param coyodata: The CoyoData
        :param ops: The operations
        :param keys: The keys
        :param language: The CoyoLanguage
        """
        super().__init__()

        logging.debug('Initializing UploadThread')

        self.coyodata = coyodata
        self.ops = ops
        self.keys = keys
        self.language = language

        self.signals = WorkerSignals()

    def _send_key(self, key, value):
        """Sends a single key to COYO, either creating it if not present or updating it if present.

        :param key: The key
        :param value: The value
        """
        if self.ops.valid:
            if not self.coyodata.prefer_update_requests:
                req_create = self.ops.create_language_key(self.language.name, key, value)
                logging.debug('Language "{}": Creating key "{}" with value "{}".'.format(self.language.name, key, value))
                if req_create[0]:
                    return True
                if not req_create[0] and req_create[1].getcode() == 401:
                    logging.info('Refreshing OAuth access token...')
                    self.ops.init()
                    if self.ops.valid:
                        logging.debug('Extracted access token: {}.'.format(self.ops.accesstoken))
                        req_create = self.ops.create_language_key(self.language.name, key, value)
                    else:
                        logging.error('Could not extract access token.')
                        return False
                if req_create[0]:
                    return True
                if not req_create[0]:
                    logging.debug('Language "{}": Updating key "{}" with value "{}".'.format(self.language.name, key, value))
                    req_update = self.ops.update_language_key(self.language.name, key, value)
                    if req_update[0]:
                        return True
                    if not req_update[0] and req_update[1].getcode() == 401:
                        logging.info('Refreshing OAuth access token.')
                        self.ops.init()
                        if self.ops.valid:
                            logging.debug('Extracted access token: {}.'.format(self.ops.accesstoken))
                            req_update = self.ops.update_language_key(self.language.name, key, value)
                        else:
                            logging.error('Could not extract access token.')
                            return False
                    if req_update[0] or (not req_update[0] and req_update[1].getcode() == 406):
                        return True
                    else:
                        return False
            else:
                req_update = self.ops.update_language_key(self.language.name, key, value)
                logging.debug('Language "{}": Updating key "{}" with value "{}".'.format(self.language.name, key, value))
                if req_update[0]:
                    return True
                if not req_update[0] and req_update[1].getcode() == 401:
                    logging.info('Refreshing OAuth access token...')
                    self.ops.init()
                    if self.ops.valid:
                        logging.debug('Extracted access token: {}.'.format(self.ops.accesstoken))
                        req_update = self.ops.update_language_key(self.language.name, key, value)
                    else:
                        logging.error('Could not extract access token.')
                        return False
                if req_update[0]:
                    return True
                if not req_update[0]:
                    logging.debug('Language "{}": Creating key "{}" with value "{}".'.format(self.language.name, key, value))
                    req_create = self.ops.create_language_key(self.language.name, key, value)
                    if req_create[0]:
                        return True
                    if not req_create[0] and req_create[1].getcode() == 401:
                        logging.info('Refreshing OAuth access token.')
                        self.ops.init()
                        if self.ops.valid:
                            logging.debug('Extracted access token: {}.'.format(self.ops.accesstoken))
                            req_create = self.ops.create_language_key(self.language.name, key, value)
                        else:
                            logging.error('Could not extract access token.')
                            return False
                    if req_create[0] or (not req_create[0] and req_create[1].getcode() == 406):
                        return True
                    else:
                        return False
        else:
            return False

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            keys_null = []
            keys_failed = {}
            currkey = 0
            self.signals.keydone.emit(currkey, keys_null, keys_failed)
            for key in self.keys:
                value = self.keys[key]
                currkey += 1
                if value:
                    if not self._send_key(key, value):
                        keys_failed[key] = value
                else:
                    keys_null.append(key)
                self.signals.keydone.emit(currkey, keys_null, keys_failed)
        except Exception as ex:
            logging.error('Failed to upload key: "{}"'.format(ex))
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(len(self.keys), keys_null, keys_failed)
        finally:
            self.signals.finished.emit()
