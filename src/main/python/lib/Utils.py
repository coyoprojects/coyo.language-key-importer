#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""Utils"""

import logging
import json

def read_json_from_file(filename_json):
    """Reads JSON from a given file

    :param filename_json: The file name
    """
    with open(filename_json, encoding='utf-8') as jsonfile:
        return json.load(jsonfile)
