#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""CoyoOps - COYO Operations"""

import logging
import json
import base64
import re
from urllib.parse import urlencode
from urllib.request import Request, urlopen
from urllib.error import HTTPError

class CoyoOps:
    """Authenticates with COYO and performs requests"""

    _urls = {
        'token': '/api/oauth/token',
        'languages': '/api/languages'
    }
    _urlparams = {
        'granttype': 'grant_type=password',
        'acctoken': 'access_token='
    }
    _urlpaths = {
        'translations': '/translations'
    }
    _additionalheaders = {}
    valid = False
    accesstoken = ''
    oauthdata = {}

    def __init__(self,
                 appconfig,
                 coyodata):
        """Initialization

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        """
        self.appconfig = appconfig
        self.coyodata = coyodata

    def init(self):
        """Retrieves the access token"""
        self._get_accesstoken()

    def create_language_key(self, language, key, value):
        """Creates a new language key

        :param language: The language
        :param key: The key
        :param value: The value
        """
        data = {
            'value': value
        }
        json_data = json.dumps(data)
        json_data_bytes = json_data.encode('utf-8')
        try:
            req = Request(self._get_create_key_url(language, key), method='POST')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Content-Length', len(json_data_bytes))
            with urlopen(req, json_data_bytes) as request:
                return (True, request.read().decode())
        except HTTPError as exception:
            return (False, exception)

    def update_language_key(self, language, key, value):
        """Updates a language key

        :param language: The language
        :param key: The key
        :param value: The value
        """
        data = {
            'value': value
        }
        json_data = json.dumps(data)
        json_data_bytes = json_data.encode('utf-8')
        try:
            req = Request(self._get_update_key_url(language, key), method='PUT')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Content-Length', len(json_data_bytes))
            with urlopen(req, json_data_bytes) as request:
                return (True, request.read().decode())
        except HTTPError as exception:
            return (False, exception)

    def _extractheaders(self, request):
        """Extracts request headers

        :param request: The request
        """
        if self.coyodata.multibackend:
            if self.coyodata.sessionname:
                logging.debug('Extracting cookie header...')
                header_setcookie = request.getheader('Set-Cookie')
                extracted = False
                if header_setcookie:
                    regex = r'' + self.coyodata.sessionname + '=(.*);'
                    matches = re.findall(regex, header_setcookie)
                    if matches:
                        self._additionalheaders['Cookie'] = self.coyodata.sessionname + '=' + matches[0]
                        extracted = True
                if not extracted:
                    self._additionalheaders['Cookie'] = None
            else:
                self._additionalheaders['Cookie'] = None
                logging.error('Could not extract cookie header. Extract the correct header "Set-Cookie" from the request:')
                logging.debug(request.info())

    def _get_gettoken_url(self):
        """Returns the 'get token' URL"""
        #unamep = '&username=' + self.username + '&password=' + self.password
        return self.coyodata.baseurl + self._urls['token'] + '?' + self._urlparams['granttype'] # + unamep

    def _get_create_key_url(self, language, key):
        """Returns the 'create key' URL

        :param language: The language
        :param key: The key
        """
        path = '/' + language + self._urlpaths['translations'] + '/' + key
        urlparams = '?' + self._urlparams['acctoken'] + self.accesstoken
        return self.coyodata.baseurl + self._urls['languages'] + path + urlparams

    def _get_update_key_url(self, language, key):
        """Returns the 'update key' URL

        :param language: The language
        :param key: The key
        """
        path = '/' + language + self._urlpaths['translations'] + '/' + key
        urlparams = '?' + self._urlparams['acctoken'] + self.accesstoken
        return self.coyodata.baseurl + self._urls['languages'] + path + urlparams

    def _retrieve_oauthdata(self):
        """Retrieves the OAuth2 data"""
        logging.debug('Retrieving OAuth2 data...')
        post_fields = {
            'username': self.coyodata.username,
            'password': self.coyodata.password
        }
        base64string = base64.b64encode('{}:{}'
                                        .format(self.coyodata.clientid, self.coyodata.clientsecret).encode('utf-8'))
        base64string_stripped = str(base64string)[2:-1]
        try:
            req = Request(self._get_gettoken_url())
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    logging.debug('Setting additional header "{}:{}"'.format(key, self._additionalheaders[key]))
                    req.add_header(key, self._additionalheaders[key])
            req.add_header('Authorization', 'Basic {}'.format(base64string_stripped))
            with urlopen(req, bytes(urlencode(post_fields).encode())) as request:
                self._extractheaders(request)
                logging.debug('Successfully retrieved OAuth2 data')
                decoded = request.read().decode('utf-8')
                self.oauthdata = json.loads(decoded)
                return True
        except HTTPError:
            logging.exception('Could not retrieve access token')
            self.valid = False

    def _get_accesstoken(self):
        """Retrieves the access token"""
        self.valid = False
        self.accesstoken = None
        if self._retrieve_oauthdata():
            logging.debug('Extracting access token...')
            try:
                self.accesstoken = self.oauthdata['access_token']
                self.valid = True
                logging.debug('Successfully extracted access token')
            except KeyError:
                logging.exception('Could not extract access token')
        return self.accesstoken
