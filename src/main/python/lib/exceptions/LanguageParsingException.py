#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""CoyoData - LanguageParsingException"""

class LanguageParsingException(Exception):
    """When the language could not be parsed"""

    def __init__(self, message):
        super().__init__(message)
